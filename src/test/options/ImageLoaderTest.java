package test.options;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.file.Paths;

import org.junit.Test;

import javafx.stage.Stage;
import utilities.ImageLoader;

public class ImageLoaderTest {

    private static final String IMAGES_DIRECTORY = Paths.get(System.getProperty("user.dir"), "res", "images").toString()
            + File.separator;

    private static final String SOUND_DIRECTORY = Paths.get(System.getProperty("user.dir"), "res", "sounds").toString()
            + File.separator;

    private static final String ALLOWED_JPG_FILEPATH = IMAGES_DIRECTORY + "mainMenu.jpg";
    private static final String ALLOWED_PNG_FILEPATH = IMAGES_DIRECTORY + "RedFury.png";
    private static final String ILLEGAL_FILE_FILEPATH = SOUND_DIRECTORY + "menu.wav";

    private static final File ALLOWED_JPG = new File(ALLOWED_JPG_FILEPATH);
    private static final File ALLOWED_PNG = new File(ALLOWED_PNG_FILEPATH);
    private static final File ILLEGAL_FILE = new File(ILLEGAL_FILE_FILEPATH);

    private static final Stage STAGE = null;
    private static final ImageLoader IMAGE_LOADER = new ImageLoader(STAGE);

    @Test
    public void onlyJpegJpgAndPngAllowed() {
        assertTrue(IMAGE_LOADER.isImage(ALLOWED_JPG));
        assertTrue(IMAGE_LOADER.isImage(ALLOWED_PNG));
        assertFalse(IMAGE_LOADER.isImage(ILLEGAL_FILE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullNotAllowed() {
        IMAGE_LOADER.isImage(null);
    }

}
