package test.options;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;

import model.game.AudioEnum;
import model.game.LevelEnum;
import utilities.ResourceMapping;

public class ResourceMappingTest {

    private static final String IMAGES_DIRECTORY = Paths.get(System.getProperty("user.dir"), "res", "images").toString()
            + File.separator;

    private static final Collection<File> EASY_BACKGROUND = Arrays.asList(
            new File(IMAGES_DIRECTORY + "backgroundEasy.jpg"), new File(IMAGES_DIRECTORY + "backgroundHard.jpg"),
            new File(IMAGES_DIRECTORY + "backgroundMedium.jpg"), new File(IMAGES_DIRECTORY + "backgroundSurvival.jpg"),
            new File(IMAGES_DIRECTORY + "backgroundSurvival.jpg"));

    private static final Collection<File> EASY_METEOR = Arrays.asList(new File(IMAGES_DIRECTORY + "meteor.png"),
            new File(IMAGES_DIRECTORY + "meteorGreen.png"), new File(IMAGES_DIRECTORY + "meteorYellow.png"),
            new File(IMAGES_DIRECTORY + "meteorRed.png"));

    private static final Collection<File> EASY_BULLET = Arrays.asList(new File(IMAGES_DIRECTORY + "bulletGreen.png"),
            new File(IMAGES_DIRECTORY + "bulletRed.png"), new File(IMAGES_DIRECTORY + "bulletPower.png"),
            new File(IMAGES_DIRECTORY + "bulletYellow.png"));

    private static final Collection<File> EASY_ENEMY = Arrays.asList(new File(IMAGES_DIRECTORY + "enemyShip1.png"),
            new File(IMAGES_DIRECTORY + "enemyShip2.png"), new File(IMAGES_DIRECTORY + "enemyShip3.png"));

    private static final List<LevelEnum> LEVELS = Arrays.asList(LevelEnum.values());
    private static final List<AudioEnum> AUDIOS = Arrays.asList(AudioEnum.values());
    private static final Set<String> AUDIO_VALUES = new HashSet<>(
            Arrays.asList("survival.wav", "menu.wav", "level.wav"));
    private static final File DEFAULT_GUEST = new File(IMAGES_DIRECTORY + "RedFury.png");
    private static final ResourceMapping RESOURCES = new ResourceMapping(LEVELS, AUDIOS);

    private static final File NEW_BACKGROUND = new File(IMAGES_DIRECTORY + "highScore.png");
    private static final File NEW_METEORS = new File(IMAGES_DIRECTORY + "bulletGreen.png");
    private static final File NEW_ENEMIES = new File(IMAGES_DIRECTORY + "bulletPower.png");
    private static final File NEW_BULLETS = new File(IMAGES_DIRECTORY + "enemyShip2.png");
    private static final File NEW_GUEST = new File(IMAGES_DIRECTORY + "explosion0.png");

    @Test
    public void initializationTest() {
        assertTrue(RESOURCES.getBackgrounds().keySet().equals(new HashSet<>(LEVELS)));
        assertTrue(RESOURCES.getMeteors().keySet().equals(new HashSet<>(LEVELS)));
        assertTrue(RESOURCES.getBullets().keySet().equals(new HashSet<>(LEVELS)));
        assertTrue(RESOURCES.getEnemies().keySet().equals(new HashSet<>(LEVELS)));

        System.out.println(RESOURCES.getBackgrounds().values());
        System.out.println(EASY_BACKGROUND);
        assertTrue(RESOURCES.getBackgrounds().values().containsAll(EASY_BACKGROUND));
        assertTrue(RESOURCES.getMeteors().values().containsAll(EASY_METEOR));
        assertTrue(RESOURCES.getBullets().values().containsAll(EASY_BULLET));
        assertTrue(RESOURCES.getEnemies().values().containsAll(EASY_ENEMY));

        assertTrue(RESOURCES.getAudios().values().stream().map(f -> f.getName()).collect(Collectors.toSet())
                .equals(AUDIO_VALUES));

        assertTrue(RESOURCES.getAudios().keySet().equals(new HashSet<>(AUDIOS)));

        assertTrue(RESOURCES.getGuest().equals(DEFAULT_GUEST));
    }

    @Test
    public void assertBackgroundInsertion() {
        final ResourceMapping rs = new ResourceMapping(LEVELS, AUDIOS);
        rs.addBackgrounds(LevelEnum.EASY, NEW_BACKGROUND);
        assertTrue(rs.getBackgrounds().values().contains(NEW_BACKGROUND));
    }

    @Test
    public void assertMeteorsInsertion() {
        final ResourceMapping rs = new ResourceMapping(LEVELS, AUDIOS);
        rs.addMeteors(LevelEnum.EASY, NEW_METEORS);
        assertTrue(rs.getMeteors().values().contains(NEW_METEORS));
    }

    @Test
    public void assertBulletsInsertion() {
        final ResourceMapping rs = new ResourceMapping(LEVELS, AUDIOS);
        rs.addBullets(LevelEnum.EASY, NEW_BULLETS);
        assertTrue(rs.getBullets().values().contains(NEW_BULLETS));
    }

    @Test
    public void assertEnemiesInsertion() {
        final ResourceMapping rs = new ResourceMapping(LEVELS, AUDIOS);
        rs.addEnemies(LevelEnum.EASY, NEW_ENEMIES);
        assertTrue(rs.getEnemies().values().contains(NEW_ENEMIES));
    }

    @Test
    public void assertGuestInsertion() {
        final ResourceMapping rs = new ResourceMapping(LEVELS, AUDIOS);
        rs.addSecondPlayer(NEW_GUEST);
        assertTrue(rs.getGuest().equals(NEW_GUEST));
    }

}
