package test.options;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.file.Paths;

import org.junit.Test;

import javafx.stage.Stage;
import utilities.AudioLoader;

public class AudioLoaderTest {

    private static final String IMAGES_DIRECTORY = Paths.get(System.getProperty("user.dir"), "res", "images").toString()
            + File.separator;

    private static final String SOUND_DIRECTORY = Paths.get(System.getProperty("user.dir"), "res", "sounds").toString()
            + File.separator;

    private static final String ALLOWED_WAV_FILEPATH = SOUND_DIRECTORY + "menu.wav";
    private static final String ILLEGAL_FILE_FILEPATH = IMAGES_DIRECTORY + "mainMenu.jpg";

    private static final File ALLOWED_WAV = new File(ALLOWED_WAV_FILEPATH);
    private static final File ILLEGAL_FILE = new File(ILLEGAL_FILE_FILEPATH);

    private static final Stage STAGE = null;
    private static final AudioLoader AUDIO_LOADER = new AudioLoader(STAGE);

    @Test
    public void onlyWavAllowed() {
        assertTrue(AUDIO_LOADER.isAudio(ALLOWED_WAV));
        assertFalse(AUDIO_LOADER.isAudio(ILLEGAL_FILE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullNotAllowed() {
        AUDIO_LOADER.isAudio(null);
    }

}
