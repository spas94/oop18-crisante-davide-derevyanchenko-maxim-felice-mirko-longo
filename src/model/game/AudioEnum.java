package model.game;

/**
 * enum for the type of the audio.
 */
public enum AudioEnum {

    /**
     * audio Menu.
     */
    MENU,
    /**
     * audio Survival.
     */
    SURVIVAL,
    /**
     * audio Game.
     */
    GAME;
}
