package model.game;

/**
 * 
 * 
 * enum for the type of the level.
 */
public enum LevelEnum {
    /**
     * level easy.
     */
    EASY("easy", 1),
    /**
     * level medium.
     */
    MEDIUM("medium", 2),
    /**
     * level hard.
     */
    HARD("hard", 3),
    /**
     * level survival.
     */
    SURVIVAL("survival", 4),
    /**
     * level multiplayer.
     */
    MULTIPLAYER("multiplayer", 5),;

    private String name;
    private final int number;

    LevelEnum(final String name, final int number) {
        this.name = name;
        this.number = number;
    }

    /**
     * 
     * @return the name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * 
     * @return the number of the level.
     */

    public int getNumber() {
        return this.number;
    }

    public LevelEnum increment() {
        switch (this) {
        case EASY:
            return MEDIUM;
        case MEDIUM:
            return HARD;
        default:
            return HARD;
        }
    }

    public boolean isIncremental() {
        return this == EASY || this == MEDIUM || this == HARD;
    }

}
