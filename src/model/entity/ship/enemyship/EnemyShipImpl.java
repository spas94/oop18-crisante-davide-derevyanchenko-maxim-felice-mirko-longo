package model.entity.ship.enemyship;

import controller.game.GameController;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import model.entity.Entity;
import model.entity.ship.AbstractShip;
import model.game.LevelEnum;
import utilities.GameUtils;

/**
 * Implementation of EnemyShip interface.
 */
public class EnemyShipImpl extends AbstractShip implements EnemyShip {

    private static final int DEFAULT_SCREEN_X_SIZE = 1920;
    private static final int DELAY = (int) (Math.random() * 600 + 200);
    private static final double POSITION_PROPORTION_VALUE = 5;
    private static final double DIMENSION_PROPORTION = 0.04;
    private static final int STARTING_LIVES = 1;
    private static final int STARTING_HEALTH = 1000;
    /**
     *  /**
     * the score point of the enemys.
     */
    public static final int SCORE_POINTS = 900;
    private final GameController gameController;
    private Point2D position;
    private final Dimension2D shipDimension;
    private final double speed;
    private int framesFromShoot;
    private LevelEnum level;
    private int framesToShoot;
    private boolean shootingAvailable;
    private boolean frozen;

    /**
     * Build a new EnemyShip.
     * 
     * @param level             is the level (fastness, power of bullets).
     * @param timeToShoot       frames passing from one bullet-shot to the next one.
     * @param fieldSize         the field width and height.
     * @param characterPosition the position of the character when the ship is
     *                          created.
     * @param myPosition        the enemyship starting position.
     * @param gameController    the gameController of the game.
     */
    public EnemyShipImpl(final LevelEnum level, final int timeToShoot, final Dimension2D fieldSize,
            final Point2D characterPosition, final Point2D myPosition, final GameController gameController) {
        super(STARTING_LIVES, (int) GameUtils.transform(STARTING_HEALTH, level.getNumber()));
        this.level = level;
        final double speedUnit = fieldSize.getWidth() / DEFAULT_SCREEN_X_SIZE;
        this.speed = speedUnit;
        this.framesToShoot = timeToShoot;
        this.frozen = false;
        double provX = Math.random() * fieldSize.getWidth();
        double provY = Math.random() * fieldSize.getHeight();
        if (Math.abs(provX - characterPosition.getX()) < fieldSize.getWidth() / POSITION_PROPORTION_VALUE) {
            provX = characterPosition.getX() + fieldSize.getWidth() / POSITION_PROPORTION_VALUE;
            if (provX < 0 || provX > fieldSize.getWidth()) {
                provX = -provX;
            }
        }
        if (Math.abs(provY - characterPosition.getY()) < fieldSize.getHeight() / POSITION_PROPORTION_VALUE) {
            provY = characterPosition.getY() + fieldSize.getHeight() / POSITION_PROPORTION_VALUE;
            if (provY < 0 || provY > fieldSize.getHeight()) {
                provY = -provY;
            }
        }
        this.position = myPosition;
        final double xSize = fieldSize.getWidth() * DIMENSION_PROPORTION;
        this.shipDimension = new Dimension2D(xSize, xSize);
        this.gameController = gameController;
    }

    /**
     * Build a new EnemyShip.
     * 
     * @param level             is the level (fastness, power of bullets).
     * @param timeToShoot       frames passing from one bullet-shot to the next one.
     * @param fieldSize         the field width and height.
     * @param characterPosition the position of the character when the ship is
     *                          created.
     * @param gameController    the gameController of the game.
     */
    public EnemyShipImpl(final LevelEnum level, final int timeToShoot, final Dimension2D fieldSize,
            final Point2D characterPosition, final GameController gameController) {
        super(STARTING_LIVES, (int) GameUtils.transform(STARTING_HEALTH, level.getNumber()));
        this.level = level;
        final double speedUnit = fieldSize.getWidth() / 1920;
        this.speed = speedUnit;
        this.framesToShoot = timeToShoot;
        this.frozen = false;
        double provX = Math.random() * fieldSize.getWidth();
        double provY = Math.random() * fieldSize.getHeight();
        if (Math.abs(provX - characterPosition.getX()) < fieldSize.getWidth() / POSITION_PROPORTION_VALUE) {
            provX = characterPosition.getX() + fieldSize.getWidth() / POSITION_PROPORTION_VALUE;
            if (provX < 0 || provX > fieldSize.getWidth()) {
                provX = -provX;
            }
        }
        if (Math.abs(provY - characterPosition.getY()) < fieldSize.getHeight() / POSITION_PROPORTION_VALUE) {
            provY = characterPosition.getY() + fieldSize.getHeight() / POSITION_PROPORTION_VALUE;
            if (provY < 0 || provY > fieldSize.getHeight()) {
                provY = -provY;
            }
        }
        this.position = new Point2D(provX, provY);
        final double xSize = fieldSize.getWidth() * DIMENSION_PROPORTION;
        this.shipDimension = new Dimension2D(xSize, xSize);
        this.gameController = gameController;
    }

    /**
     * Build a customizable level Bullet.
     * 
     * @param level             is the level (fastness, power of bullets).
     * @param fieldSize         the field width and height.
     * @param characterPosition the position of the character when the ship is
     *                          created.
     * @param gameController    the gameController of the game.
     */
    public EnemyShipImpl(final LevelEnum level, final Dimension2D fieldSize, final Point2D characterPosition,
            final GameController gameController) {
        this(level, DELAY, fieldSize, characterPosition, gameController);
    }

    /**
     * Build a simple EnemyShip.
     * 
     * @param fieldSize         the field width and height.
     * @param characterPosition the position of the character when the ship is
     *                          created.
     * @param gameController    the gameController of the game.
     */
    public EnemyShipImpl(final Dimension2D fieldSize, final Point2D characterPosition, final GameController gameController) {
        this(gameController.getGameLevel(), fieldSize, characterPosition, gameController);
    }

    /**
     * Get the difficulty level.
     * 
     * @return the level.
     */
    public LevelEnum getLevel() {
        return this.level;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canShoot() {
        return this.shootingAvailable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean intersects(final Entity entity) {
        final boolean isIntersected = entity.getBoundary().intersects(this.getBoundary());
        if (isIntersected) {
            this.destroy();
            entity.destroy();
            this.gameController.destroyedBy(entity, SCORE_POINTS);
        }
        return isIntersected;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized Point2D shoot() {
        this.framesFromShoot = 0;
        this.shootingAvailable = false;
        return this.position.add(this.shipDimension.getWidth() / 2, this.shipDimension.getHeight() / 2);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(final Point2D position) {
        if (!frozen) {
            this.position = this.position.add(position);
            this.framesFromShoot++;
            if (framesFromShoot >= framesToShoot) {
                this.shootingAvailable = true;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getSpeed() {
        return this.speed;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected synchronized Point2D getPosition() {
        return this.position;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Dimension2D getDimension() {
        return new Dimension2D(this.shipDimension.getWidth(), this.shipDimension.getHeight());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getScorePoints() {
        return SCORE_POINTS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void setFreeze(final boolean value) {
        this.frozen = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized boolean isFrozen() {
        return this.frozen;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPosition(final Point2D position) {
        this.position = position;
    }
}
