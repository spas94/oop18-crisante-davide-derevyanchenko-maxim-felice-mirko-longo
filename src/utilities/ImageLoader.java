package utilities;

import java.io.File;
import java.nio.file.Paths;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ImageLoader {

    private static final File RESOURCE_DIRECTORY = new File(
            Paths.get(System.getProperty("user.dir"), "res", "images").toString());

    private final FileChooser chooser;
    private final Stage stage;

    public ImageLoader(final Stage stage) {
        this.chooser = new FileChooser();
        chooser.setInitialDirectory(RESOURCE_DIRECTORY);
        this.stage = stage;
    }

    public final File loadImage() {
        File file;
        chooser.setTitle("Open Resource File");
        file = chooser.showOpenDialog(this.stage);
        if (file != null && isImage(file)) {
            return file;
        }
        return null;
    }

    public final boolean isImage(final File file) {
        if (file == null) {
            throw new IllegalArgumentException("The file is null");
        }
        final String name = file.getName();
        return name.endsWith("png") || name.endsWith("jpeg") || name.endsWith("jpg");
    }
}
