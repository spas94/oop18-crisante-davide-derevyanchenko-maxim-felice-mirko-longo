package utilities;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;

import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.Effect;

/**
 * 
 * This class contains all game utilities.
 *
 */
public final class GameUtils {

    private static final int BLUR_EFFECT_RANGE = 5;
    private static final Effect TRANSPARENT = new BoxBlur(0, 0, 0);
    private static final Effect BLUR = new BoxBlur(BLUR_EFFECT_RANGE, BLUR_EFFECT_RANGE, BLUR_EFFECT_RANGE);
    private static AudioClip menu = Applet.newAudioClip(ClassLoader.getSystemResource("menu.wav"));
    private static AudioClip survival = Applet.newAudioClip(ClassLoader.getSystemResource("survival.wav"));
    private static AudioClip level = Applet.newAudioClip(ClassLoader.getSystemResource("level.wav"));

    private GameUtils() {
    }

    /**
     * Method to mute all the sounds in the game.
     */
    public static void muteAllSounds() {
        menu.stop();
        survival.stop();
        level.stop();
    }

    public static void setMenuAudio(final File audio) {
        try {
            menu.stop();
            menu = Applet.newAudioClip(audio.toURI().toURL());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static void setSurvivalAudio(final File audio) {
        try {
            survival.stop();
            survival = Applet.newAudioClip(audio.toURI().toURL());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static void setLevelAudio(final File audio) {
        try {
            level.stop();
            level = Applet.newAudioClip(audio.toURI().toURL());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the sound of the Menu.
     * 
     * @return the sound of menu
     */
    public static AudioClip getMenuMusic() {
        return menu;
    }

    /**
     * Get the survival sound of the Game.
     * 
     * @return the sound of the survival
     */
    public static AudioClip getSurvivalMusic() {
        return survival;
    }

    /**
     * Get the level sound of the Game.
     * 
     * @return the sound of the level
     */
    public static AudioClip getLevelMusic() {
        return level;
    }

    /**
     * Get the transparent Effect.
     * 
     * @return the effect
     */
    public static Effect getTransparentEffect() {
        return TRANSPARENT;
    }

    /**
     * Get the blur Effect.
     * 
     * @return the effect
     */
    public static Effect getBlurEffect() {
        return BLUR;
    }

    /**
     * Method that transforms a value depending on the level of the entity.
     * 
     * @param value the value to be transformed
     * @param level the level to be used in transformation
     * @return the modified value
     */
    public static int transform(final int value, final int level) {
        return level * value;
    }

}
