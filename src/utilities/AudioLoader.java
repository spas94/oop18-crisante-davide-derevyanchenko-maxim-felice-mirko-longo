package utilities;

import java.io.File;
import java.nio.file.Paths;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class AudioLoader {

    private static final File RESOURCE_DIRECTORY = new File(
            Paths.get(System.getProperty("user.dir"), "res", "sounds").toString());

    private final FileChooser chooser;
    private final Stage stage;

    public AudioLoader(final Stage stage) {
        this.chooser = new FileChooser();
        chooser.setInitialDirectory(RESOURCE_DIRECTORY);
        this.stage = stage;
    }

    public final File loadAudio() {
        File file;
        chooser.setTitle("Open Resource File");
        file = chooser.showOpenDialog(this.stage);
        if (file != null && isAudio(file)) {
            return file;
        }
        return null;
    }

    public final boolean isAudio(final File file) {
        if (file == null) {
            throw new IllegalArgumentException("The file is null");
        }
        final String name = file.getName();
        return name.endsWith("wav");
    }
}
