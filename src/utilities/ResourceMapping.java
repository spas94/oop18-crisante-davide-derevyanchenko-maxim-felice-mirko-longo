package utilities;

import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.game.AudioEnum;
import model.game.LevelEnum;

public class ResourceMapping {

    private static final File RESOURCE_DIRECTORY_IMAGES = new File(
            Paths.get(System.getProperty("user.dir"), "res", "images").toString());
    private static final String IMAGES = RESOURCE_DIRECTORY_IMAGES.toString() + File.separator;

    private static final File RESOURCE_DIRECTORY_AUDIO = new File(
            Paths.get(System.getProperty("user.dir"), "res", "sounds").toString());

    private static final String AUDIO = RESOURCE_DIRECTORY_AUDIO.toString() + File.separator;

    private final Map<LevelEnum, File> backgrounds;
    private final Map<LevelEnum, File> meteors;
    private final Map<LevelEnum, File> bullets;
    private final Map<LevelEnum, File> enemies;
    private final Map<AudioEnum, File> audios;
    private File fileGuest;

    public ResourceMapping(final List<LevelEnum> levels, final List<AudioEnum> audioType) {
        backgrounds = new HashMap<>();
        meteors = new HashMap<>();
        bullets = new HashMap<>();
        enemies = new HashMap<>();
        audios = new HashMap<>();
        fileGuest = new File(IMAGES + "RedFury.png");

        for (final LevelEnum level : levels) {
            backgrounds.put(level, null);
            meteors.put(level, null);
            bullets.put(level, null);
            enemies.put(level, null);

        }
        for (final AudioEnum audio : audioType) {
            audios.put(audio, null);
        }

        initWithDefault();
    }

    private void initWithDefault() {
        backgrounds.put(LevelEnum.EASY, new File(IMAGES + "backgroundEasy.jpg"));
        backgrounds.put(LevelEnum.MEDIUM, new File(IMAGES + "backgroundMedium.jpg"));
        backgrounds.put(LevelEnum.HARD, new File(IMAGES + "backgroundHard.jpg"));
        backgrounds.put(LevelEnum.SURVIVAL, new File(IMAGES + "backgroundSurvival.jpg"));
        backgrounds.put(LevelEnum.MULTIPLAYER, new File(IMAGES + "backgroundSurvival.jpg"));

        meteors.put(LevelEnum.EASY, new File(IMAGES + "meteor.png"));
        meteors.put(LevelEnum.MEDIUM, new File(IMAGES + "meteorGreen.png"));
        meteors.put(LevelEnum.HARD, new File(IMAGES + "meteorRed.png"));
        meteors.put(LevelEnum.SURVIVAL, new File(IMAGES + "meteorYellow.png"));
        meteors.put(LevelEnum.MULTIPLAYER, new File(IMAGES + "meteorYellow.png"));

        bullets.put(LevelEnum.EASY, new File(IMAGES + "bulletRed.png"));
        bullets.put(LevelEnum.MEDIUM, new File(IMAGES + "bulletGreen.png"));
        bullets.put(LevelEnum.HARD, new File(IMAGES + "bulletPower.png"));
        bullets.put(LevelEnum.SURVIVAL, new File(IMAGES + "bulletYellow.png"));
        bullets.put(LevelEnum.MULTIPLAYER, new File(IMAGES + "bulletYellow.png"));

        enemies.put(LevelEnum.EASY, new File(IMAGES + "enemyShip1.png"));
        enemies.put(LevelEnum.MEDIUM, new File(IMAGES + "enemyShip2.png"));
        enemies.put(LevelEnum.HARD, new File(IMAGES + "enemyShip3.png"));
        enemies.put(LevelEnum.SURVIVAL, new File(IMAGES + "enemyShip1.png"));
        enemies.put(LevelEnum.MULTIPLAYER, new File(IMAGES + "enemyShip2.png"));

        audios.put(AudioEnum.SURVIVAL, new File(AUDIO + "survival.wav"));
        audios.put(AudioEnum.MENU, new File(AUDIO + "menu.wav"));
        audios.put(AudioEnum.GAME, new File(AUDIO + "level.wav"));

    }

    public final void addAudioGame(final File file) {
        audios.put(AudioEnum.GAME, file);
    }

    public final void addAudioOption(final File file) {
        audios.put(AudioEnum.SURVIVAL, file);
    }

    public final void addAudioMenu(final File file) {
        audios.put(AudioEnum.MENU, file);
    }

    public final void addBackgrounds(final LevelEnum level, final File file) {
        backgrounds.put(level, file);
    }

    public final void addEnemies(final LevelEnum level, final File file) {
        enemies.put(level, file);
    }

    public final void addMeteors(final LevelEnum level, final File file) {
        meteors.put(level, file);
    }

    public final void addBullets(final LevelEnum level, final File file) {
        bullets.put(level, file);
    }

    public final Map<LevelEnum, File> getBackgrounds() {
        return this.backgrounds;
    }

    public final Map<AudioEnum, File> getAudios() {
        return this.audios;
    }

    public final Map<LevelEnum, File> getMeteors() {
        return this.meteors;
    }

    public final Map<LevelEnum, File> getBullets() {
        return this.bullets;
    }

    public final Map<LevelEnum, File> getEnemies() {
        return this.enemies;
    }

    public final File getGuest() {
        return this.fileGuest;
    }

    public final void addSecondPlayer(final File file) {
        this.fileGuest = file;
    }
}
