package controller.multiplayer;

import controller.game.GameController;
import controller.game.field.entities.BulletController;
import controller.game.field.entities.CharacterController;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;

/**
 * Class that controls the character ship moves.
 *
 */
public class SecondCharacterController extends CharacterController {

    private double angleDifferential;
    private final Image shipImage;
    private static final int MAX_LEGAL_DEGREE = 360;
    private static final int MIN_LEGAL_DEGREE = 0;
    private static final double TURNE_ANGLE = 0.3;
    private static final int LEGAL_DEGREE = 359;

    /**
     * Constructor of the CharacterController.
     * 
     * @param gameController  the view in which the ship is moving
     * @param initialPosition the initial position of the ship
     */
    public SecondCharacterController(final GameController gameController, final Point2D initialPosition) {
        super(gameController, initialPosition, Player.SECOND);
        this.angleDifferential = MIN_LEGAL_DEGREE;
        this.shipImage = new Image("file:///" + gameController.getAccount().getSettings().getResourceMapping().getGuest());


    }

    private synchronized void setAngleDifferential(final double angleDifferential) {
        this.angleDifferential = angleDifferential;
    }

    /**
     * 
     */
    public final void incrementAngle() {
        if (angleDifferential > LEGAL_DEGREE) {
            setAngleDifferential(MIN_LEGAL_DEGREE);
        } else {
            setAngleDifferential(angleDifferential + TURNE_ANGLE);
        }
    }

    /**
     * decrement the angle of the second player.
     */
    public final void decrementAngle() {
        if (angleDifferential < MIN_LEGAL_DEGREE) {
            setAngleDifferential(MAX_LEGAL_DEGREE - 1);
        } else {
            setAngleDifferential(angleDifferential - TURNE_ANGLE);
        }
    }

    /**
     * 
     * @return the updatePosition.
     */
    public final Point2D getUpdatedPosition() {
        final long time = System.currentTimeMillis();
        final long timeFromLastUpdate = time - this.getLastUpdate();
        setLastUpdate(time);
        setAngle(Math.toDegrees(angleDifferential));
        double shipUpdateX = this.getShip().getBoundary().getMinX()
                + (timeFromLastUpdate * this.getShip().getSpeed() * Math.cos(angleDifferential));
        double shipUpdateY = this.getShip().getBoundary().getMinY()
                + (timeFromLastUpdate * this.getShip().getSpeed() * Math.sin(angleDifferential));
        if (shipUpdateX < 0) {
            shipUpdateX = this.getGameController().getFieldView().getCanvas().getWidth()
                    - this.getShip().getBoundary().getWidth();
        } else if (shipUpdateX > (this.getGameController().getFieldView().getCanvas().getWidth()
                - this.getShip().getBoundary().getWidth())) {
            shipUpdateX = 0;
        }
        if (shipUpdateY < 0) {
            shipUpdateY = this.getGameController().getFieldView().getCanvas().getHeight()
                    - this.getShip().getBoundary().getHeight();
        } else if (shipUpdateY > (this.getGameController().getFieldView().getCanvas().getHeight()
                - this.getShip().getBoundary().getHeight())) {
            shipUpdateY = 0;
        }

        return new Point2D(shipUpdateX, shipUpdateY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update() {

        final Point2D updatedPosition = getUpdatedPosition();
        this.getGameController().getFieldController().getMultiCamera()
                .setTranslation(new Point2D(updatedPosition.getX() - this.getShip().getBoundary().getMinX(),
                        updatedPosition.getY() - this.getShip().getBoundary().getMinY()));
        this.getShip().update(updatedPosition.getX(), updatedPosition.getY());
    }

    /**
     * Method that creates a second character's bullet. 
     */
    public synchronized void shoot() {
        final double rad = Math.toRadians(this.getAngle());
        final Point2D startingPoint = this.getShip().getCentralPosition().add(
                Math.cos(rad) * (this.getShip().getBoundary().getHeight() * 0.5),
                Math.sin(rad) * (this.getShip().getBoundary().getHeight() * 0.5));
        this.getGameController().getFieldController().addCharacterBullet(new BulletController(this.getGameController(),
                getBulletLevel(), this.getShip().getCentralPosition(), startingPoint, this.getResolution(), Player.SECOND));
    }

    @Override
    public final void draw() {
        this.getGameController().getFieldView().drawEntity(this.shipImage, getAngle(), getShip().getBoundary());
    }

    @Override
    public final Image getShipImage() {
        return this.shipImage;
    }
}
