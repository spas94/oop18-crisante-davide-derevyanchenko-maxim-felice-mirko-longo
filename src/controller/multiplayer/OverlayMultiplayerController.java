package controller.multiplayer;

import controller.StageController;
import controller.game.GameController;
import controller.game.OverlayController;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import model.account.Account;

/**
 * The multiplayer player overlay.
 */
public class OverlayMultiplayerController extends OverlayController {

    /**
     * is a lives of the second character ship.
     */
    public static final String LIVES2 = "LIVES2";
    /**
     * the energy of the second character ship .
     */
    public static final String HP2 = "HP2";
    /**
     * the score of the second character ship.
     */
    public static final String SCORE2 = "SCORE2";

    @FXML
    private Label lives2;
    @FXML
    private Label hp2;
    @FXML
    private Label score2;

    /**
     * 
     * @param account         the account.
     * @param stageController the Stage controller.
     * @param gameController  the game controller.
     * @param multiplayer     the multiplayer controller.
     */
    public OverlayMultiplayerController(final Account account, final StageController stageController,
            final GameController gameController, final boolean multiplayer) {
        super(account, stageController, gameController, multiplayer);
    }

    /**
     * initialize of the overlay the multiplayer system.
     */
    protected void initLabelSystem() {
        Platform.runLater(() -> {
            getLives().setText(getBundle().getString(LIVES)
                    + Integer.toString(getGameController().getFieldController().getCharacter().getLife().getLives()));
            getHp().setText(getBundle().getString(HP) + Integer
                    .toString(getGameController().getFieldController().getCharacter().getLife().getCurrentHealth()));
            getScore().setText(
                    getBundle().getString(SCORE) + Integer.toString(getGameController().getScore().getScorePoints()));

            lives2.setText(getBundle().getString(LIVES2) + Integer
                    .toString(getGameController().getFieldController().getSecondPlayer().getLife().getLives()));
            hp2.setText(getBundle().getString(HP) + Integer
                    .toString(getGameController().getFieldController().getSecondPlayer().getLife().getCurrentHealth()));
            score2.setText(getBundle().getString(SCORE)
                    + Integer.toString(getGameController().getSecondPlayerScore().getScorePoints()));
        });
    }
}
