package controller.multiplayer;
    /**
     * enum to the player.
     */
public enum Player {
    /**
     * first player.
     */
    FIRST, 
    /**
     * second player.
     */
    SECOND,
    /**
     * enemy player.
     */
    ENEMY
}
