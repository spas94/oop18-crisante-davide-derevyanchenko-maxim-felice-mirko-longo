package controller.agents;

import controller.game.GameController;
import model.game.LevelEnum;
import utilities.GameUtils;

/**
 * Class that represents the game thread that constantly creates spawn agents of
 * the appropriate level.
 */
public class GameAgent extends Thread {

    private final LevelEnum gameLevel;
    private final GameController gameContoller;
    private final boolean isMultiplayer;

    /**
     * Constructor for the GameAgent.
     * 
     * @param gameController the controller of the game.
     * @param gameLevel      the difficulty of the game.
     * @param multiplayer    the multiplayer of the game.
     */
    public GameAgent(final GameController gameController, final LevelEnum gameLevel, final boolean multiplayer) {
        this.gameContoller = gameController;
        this.gameLevel = gameLevel;
        this.isMultiplayer = multiplayer;
    }

    /**
     * {@inheritDoc}
     */
 
    @Override
    public void run() {
        if (this.gameContoller.getAccount().getSettings().isSoundOn()) {
            GameUtils.getLevelMusic().loop();
        }
        new SpawnAgent(this.gameContoller, this.gameLevel,
                this.gameContoller.getAccount().getSettings().getResolution(), isMultiplayer).start();
    }
}
