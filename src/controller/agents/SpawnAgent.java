package controller.agents;

import java.util.Random;

import controller.game.GameController;
import controller.game.field.FieldController;
import controller.game.field.entities.CharacterController;
import controller.game.field.entities.EnemyController;
import controller.game.field.entities.MeteorController;
import javafx.geometry.Dimension2D;
import model.entity.ship.charactership.CharacterShip;
import model.game.LevelEnum;
import utilities.ErrorLog;

/**
 * 
 * Agent that administers the enemies spawns.
 *
 */
public class SpawnAgent extends Thread {

    private static final int WAITING_TIME = 1500;
    private static final int BOUND = 8000;
    private static final int SCORE_LEVEL_MULTIPLIER = 70000;
    private final int scoreLimit;
    private final CharacterController firstController;
    private final CharacterController secondController;
    private final GameController gameController;
    private final LevelEnum level;
    private final Dimension2D fieldSize;
    private final FieldController fieldController;
    private double meteorWaiting;

    private final boolean isMultiplayer;

    /**
     * Build the SpawnAgent.
     * 
     * @param gameController the game controller.
     * @param level          the game level.
     * @param fieldSize      the field width and height.
     * @param multiplayer    the controller of the multiplayer.
     */

    public SpawnAgent(final GameController gameController, final LevelEnum level, final Dimension2D fieldSize,
            final boolean multiplayer) {
        this.gameController = gameController;
        this.level = level;
        this.scoreLimit = this.level.getNumber() * SCORE_LEVEL_MULTIPLIER;
        this.fieldSize = fieldSize;
        this.fieldController = this.gameController.getFieldController();
        this.firstController = fieldController.getCharacter();
        if (multiplayer) {
            this.secondController = fieldController.getSecondPlayer();
        } else {
            this.secondController = null;
        }
        this.isMultiplayer = multiplayer;
        this.meteorWaiting = (Math.random() * BOUND) + WAITING_TIME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        while (!this.gameController.isEnded() && this.gameController.getScore().getScorePoints() < this.scoreLimit) {
            try {
                if (!this.gameController.isInPause()) {
                    this.meteorWaiting -= WAITING_TIME;
                    if (this.meteorWaiting < 0) {
                        if (this.isMultiplayer) {
                            final Double coinFlip = new Random().nextDouble();
                            if (coinFlip > 0.5) {
                                this.fieldController.addMeteor(new MeteorController(gameController, this.level,
                                        ((CharacterShip) (firstController.getEntity())).getCentralPosition(),
                                        this.fieldSize));
                                this.meteorWaiting = (Math.random() * BOUND) + WAITING_TIME;
                            } else {
                                this.fieldController.addMeteor(new MeteorController(gameController, this.level,
                                        ((CharacterShip) (secondController.getEntity())).getCentralPosition(),
                                        this.fieldSize));
                                this.meteorWaiting = (Math.random() * BOUND) + WAITING_TIME;
                            }
                        } else {
                            this.fieldController.addMeteor(new MeteorController(gameController, this.level,
                                    ((CharacterShip) (firstController.getEntity())).getCentralPosition(),
                                    this.fieldSize));
                            this.meteorWaiting = (Math.random() * BOUND) + WAITING_TIME;
                        }
                    }
                    if (this.isMultiplayer) {
                        final Double coinFlip = new Random().nextDouble();
                        if (coinFlip > 0.5) {
                            final EnemyController enemyController = new EnemyController(gameController, this.level,
                                    firstController, this.fieldSize);
                            enemyController.getEntity().setFreeze(this.gameController.isFrozen());
                            this.fieldController.addEnemy(enemyController);
                        } else {
                            final EnemyController enemyController = new EnemyController(gameController, this.level,
                                    secondController, this.fieldSize);
                            enemyController.getEntity().setFreeze(this.gameController.isFrozen());
                            this.fieldController.addEnemy(enemyController);
                        }
                    } else {
                        final EnemyController enemyController = new EnemyController(gameController, this.level,
                                firstController, this.fieldSize);
                        enemyController.getEntity().setFreeze(this.gameController.isFrozen());
                        this.fieldController.addEnemy(enemyController);
                    }

                }
                Thread.sleep(WAITING_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();

                ErrorLog.getLog().printError();
                System.exit(0);
            }
        }
        if (this.gameController.getGameLevel().getNumber() < 4) {
            this.gameController.setEnded(true);
        }
    }
}
