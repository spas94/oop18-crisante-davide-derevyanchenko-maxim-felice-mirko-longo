package controller.game;

import controller.StageController;
import controller.agents.GameAgent;
import controller.agents.PowerUpAgent;
import controller.agents.entities.BulletAgent;
import controller.game.field.CameraController;
import controller.game.field.FieldController;
import controller.menu.OverlaySinglePlayerController;
import controller.multiplayer.OverlayMultiplayerController;
import controller.multiplayer.Player;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.WindowEvent;
import model.account.Account;
import model.entity.Entity;
import model.entity.bullet.BulletImpl;
import model.entity.ship.charactership.CharacterShipImpl;
import model.game.LevelEnum;
import model.game.Score;
import model.game.ScoreImpl;
import utilities.GameUtils;
import view.game.FieldView;
import view.game.GameView;

/**
 * Class that represents the controller of the game.
 *
 */
public class GameController {

    private static final int WIN_CONDITIONS = 15000;
    private static final double SINGLE_SCALE_VALUE = 0.75;
    private static final double MULTI_SCALE_VALUE = 1.0;
    private final FieldController fieldController;
    private final OverlayController overlayController;
    private final StageController stageController;
    private final PowerUpController powerController;
    private final GameView gameView;
    private final FieldView fieldView;
    private final Score score;
    private final Score secondPlayerScore;
    private final Account account;
    private final LevelEnum gameLevel;
    private boolean inPause;
    private boolean ended;
    private boolean frozen;
    private final boolean multiplayer;

    /**
     * Constructor of the GameController.
     * 
     * @param account         the account of the player
     * @param stageController the controller of the Stage
     * @param gameLevel       the difficulty of the game
     */
    public GameController(final Account account, final StageController stageController, final LevelEnum gameLevel) {
        this.gameLevel = gameLevel;
        this.ended = false;
        this.account = account;
        this.stageController = stageController;
        this.powerController = new PowerUpController(this);
        this.gameView = new GameView(stageController);
        this.score = new ScoreImpl();
        final GameController controller = this;
        if (gameLevel == LevelEnum.MULTIPLAYER) {
            this.multiplayer = true;
        } else {
            this.multiplayer = false;
        }
        this.stageController.setTutorialHintStageController(multiplayer);

        final EventHandler<KeyEvent> exitHandler = new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent event) {
                if (event.getCode().compareTo(KeyCode.ESCAPE) == 0) {
                    startHandler();
                }
                if (multiplayer) {
                    if (event.getCode().compareTo(KeyCode.D) == 0) {
                        rightHandler();
                    }
                    if (event.getCode().compareTo(KeyCode.A) == 0) {
                        leftHandler();
                    }
                    if (event.getCode().compareTo(KeyCode.SPACE) == 0) {
                        fieldController.getSecondPlayer().shoot();
                    }
                }
            }
        };
        final EventHandler<MouseEvent> shootHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(final MouseEvent event) {
                fieldController.getCharacter().shoot();
            }
        };
        final EventHandler<MouseEvent> exitSceneHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(final MouseEvent event) {
                startHandler();
            }
        };
        final EventHandler<WindowEvent> exitWindow = new EventHandler<WindowEvent>() {
            @Override
            public void handle(final WindowEvent event) {
                controller.ended = true;
                GameUtils.muteAllSounds();
                Platform.exit();
            }
        };
        this.stageController.setWindowHandler(exitWindow);
        GameUtils.muteAllSounds();
        this.gameView.getScene().setOnKeyPressed(exitHandler);
        this.gameView.getScene().setOnMouseExited(exitSceneHandler);
        this.gameView.getScene().setOnMousePressed(shootHandler);
        this.fieldView = new FieldView(this.account.getSettings().getResolution(), this.gameLevel, this.account);
        stageController.setScene(this.gameView.getScene());
        this.gameView.getRoot().getChildren().add(this.fieldView.getSubScene());
        if (gameLevel == LevelEnum.MULTIPLAYER) {
            this.fieldController = new FieldController(this, MULTI_SCALE_VALUE);
            this.overlayController = new OverlayMultiplayerController(account, stageController, this, multiplayer);
            this.secondPlayerScore = new ScoreImpl();
        } else {
            this.fieldController = new FieldController(this, new CameraController(this.getFieldView().getCamera()),
                    SINGLE_SCALE_VALUE);
            this.overlayController = new OverlaySinglePlayerController(account, stageController, this, multiplayer);
            this.secondPlayerScore = null;
        }
        this.overlayController.start();
        this.startAgent(new BulletAgent(this));
        this.startAgent(new PowerUpAgent(this));
        this.startAgent(new GameAgent(this, this.gameLevel, this.multiplayer));
        this.stageController.setFullScreen(true);
    }

    private void rightHandler() {
        this.fieldController.getSecondPlayer().decrementAngle();
    }

    private void leftHandler() {
        this.fieldController.getSecondPlayer().incrementAngle();
    }

    public final boolean isGameOver() {
        return this.fieldController.isGameOver();
    }

    /**
     * Get the PowerUpController.
     * 
     * @return the PowerUpController
     */
    public PowerUpController getPowerController() {
        return this.powerController;
    }

    /**
     * Get the Score.
     * 
     * @return the score
     */
    public Score getScore() {
        return this.score;
    }

    /**
     * Gets the FieldController of this game.
     * 
     * @return the FieldController
     */
    public FieldController getFieldController() {
        return this.fieldController;
    }

    /**
     * Sets the value of the inPause state.
     * 
     * @param inPause the value that says if the game is paused or not
     */
    public synchronized void setInPause(final boolean inPause) {
        this.inPause = inPause;
    }

    /**
     * Method that says if the game is in pause or not.
     * 
     * @return true if the game is in pause, false otherwise
     */
    public synchronized boolean isInPause() {
        return this.inPause;
    }

    /**
     * Sets the value of the ended state.
     * 
     * @param ended the value that says if the game is ended or not
     */
    public synchronized void setEnded(final boolean ended) {
        this.ended = ended;
        Platform.runLater(() -> new GameOverController(this.account, this.stageController, this).start());
    }

    /**
     * Method that says if the game is ended or not.
     * 
     * @return true if the game is ended, false otherwise
     */
    public synchronized boolean isEnded() {
        return this.ended;
    }

    /**
     * Set the frozen status.
     * 
     * @param value the value to set
     */
    public synchronized void setFrozen(final boolean value) {
        this.frozen = value;
    }

    /**
     * Check the enemies' frozen status, if there aren't enemies the value is set to
     * false.
     * 
     * @return the enemies' frozen status
     */
    public synchronized boolean isFrozen() {
        return this.frozen;
    }

    /**
     * Get the game view.
     * 
     * @return the game view
     */
    public GameView getGameView() {
        return this.gameView;
    }

    /**
     * Get the fieldView.
     * 
     * @return the fieldView
     */
    public FieldView getFieldView() {
        return this.fieldView;
    }

    /**
     * Get the overlayController.
     * 
     * @return the overlayController
     */
    public OverlayController getOverlayController() {
        return this.overlayController;
    }

    /**
     * Method that returns the actual account that is in game.
     * 
     * @return the account that is in game
     */
    public Account getAccount() {
        return this.account;
    }

    /**
     * Method that gets the difficulty of the game.
     * 
     * @return the level of the game
     */
    public LevelEnum getGameLevel() {
        return this.gameLevel;
    }

    private void startAgent(final Thread agent) {
        agent.start();
    }

    private void startHandler() {
        if (!this.inPause && !this.ended) {
            this.inPause = true;
            GameUtils.muteAllSounds();
            new PauseController(this.account, this.stageController, this).start();
        }
    }

    public final boolean isMultiplayer() {
        return multiplayer;
    }

    public final Score getSecondPlayerScore() {
        return secondPlayerScore;
    }

    public final void destroyedBy(final Entity entity, final int score) {
        if (entity instanceof CharacterShipImpl) {
            final CharacterShipImpl player = (CharacterShipImpl) entity;
            if (player.getPlayer() == Player.FIRST) {
                this.score.addScore(score);
            } else if (player.getPlayer() == Player.SECOND) {
                this.secondPlayerScore.addScore(score);
            }
        } else if (entity instanceof BulletImpl) {
            final BulletImpl player = (BulletImpl) entity;
            if (player.getShooter() == Player.FIRST) {
                this.score.addScore(score);
            } else if (player.getShooter() == Player.SECOND) {
                this.secondPlayerScore.addScore(score);
            }
        }
        if (multiplayer) {
            if (this.score.getScorePoints() > WIN_CONDITIONS) {
                fieldController.getSecondPlayer().destroy();
            }
            if (this.secondPlayerScore.getScorePoints() > WIN_CONDITIONS) {
                fieldController.getCharacter().destroy();
            }
        }
    }
}
