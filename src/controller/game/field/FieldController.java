package controller.game.field;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import controller.agents.DrawAgent;
import controller.agents.entities.AntagonistsAgent;
import controller.agents.entities.CharacterAgent;
import controller.agents.entities.CharacterBulletAgent;
import controller.game.GameController;
import controller.game.field.entities.BulletController;
import controller.game.field.entities.CharacterController;
import controller.game.field.entities.EnemyController;
import controller.game.field.entities.MeteorController;
import controller.game.field.entities.SinglePlayerController;
import controller.multiplayer.SecondCharacterController;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import utilities.ErrorLog;

/**
 * The field controller's class.
 *
 */
public class FieldController {

    private static final Point2D FIRST_PLAYER_INITIAL_POSITION = new Point2D(200, 200);
    private static final Point2D SECOND_PLAYER_INITIAL_POSITION = new Point2D(600, 600);

    private final CharacterController firstPlayer;
    private final SecondCharacterController secondPlayer;
    private final List<EnemyController> enemies;
    private final List<BulletController> enemyBullets;
    private final List<BulletController> characterBullets;
    private final List<MeteorController> meteors;
    private final GameController gameController;
    private final DrawAgent drawAgent;
    private final CameraController multiplayerCamera;

    /**
     * Constructor of the FieldController.
     * 
     * @param gameController the GameController of this session
     * @param scaleValue     the value of scale the game
     */
    public FieldController(final GameController gameController, final double scaleValue) {
        this.gameController = gameController;
        final Dimension2D resolution = this.gameController.getAccount().getSettings().getResolution();
        this.enemies = Collections.synchronizedList(new LinkedList<>());
        this.enemyBullets = Collections.synchronizedList(new LinkedList<>());
        this.characterBullets = Collections.synchronizedList(new LinkedList<>());
        this.meteors = Collections.synchronizedList(new LinkedList<>());
        final CameraController camController = new CameraController(this.gameController.getFieldView().getCamera());
        camController.setCam(scaleValue, resolution);
        this.firstPlayer = new SinglePlayerController(gameController, FIRST_PLAYER_INITIAL_POSITION);
        this.secondPlayer = new SecondCharacterController(this.gameController, SECOND_PLAYER_INITIAL_POSITION);
        this.multiplayerCamera = camController;
        this.drawAgent = new DrawAgent(this.gameController, camController, true);
        this.startAgent(this.drawAgent);
        this.startAgent(new CharacterAgent(this.firstPlayer, this.gameController));
        this.startAgent(new CharacterAgent(this.secondPlayer, this.gameController));
    }

    /**
     * Constructor of the FieldController.
     * 
     * @param gameController the GameController of this session
     * @param camController  the CameraController of this session
     * @param scaleValue     the value of scale the game
     * 
     */
    public FieldController(final GameController gameController, final CameraController camController,
            final double scaleValue) {
        this.gameController = gameController;
        final Dimension2D resolution = this.gameController.getAccount().getSettings().getResolution();
        this.enemies = Collections.synchronizedList(new LinkedList<>());
        this.enemyBullets = Collections.synchronizedList(new LinkedList<>());
        this.characterBullets = Collections.synchronizedList(new LinkedList<>());
        this.meteors = Collections.synchronizedList(new LinkedList<>());
        camController.setCam(scaleValue, resolution);
        this.firstPlayer = new SinglePlayerController(this.gameController, camController);
        this.secondPlayer = null;
        this.multiplayerCamera = null;
        this.drawAgent = new DrawAgent(this.gameController, camController);
        this.startAgent(this.drawAgent);
        this.startAgent(new CharacterAgent(this.firstPlayer, this.gameController));
        try {
            new Robot().mouseMove((int) resolution.getWidth() / 2, (int) resolution.getHeight() / 2);
        } catch (AWTException e) {
            e.printStackTrace();

            ErrorLog.getLog().printError();
            System.exit(0);
        }
    }

    public final boolean isGameOver() {
        throw new IllegalStateException("game over not implementes");
    }

    /**
     * Gets the GameController of this session.
     * 
     * @return the GameController
     */
    public GameController getGameController() {
        return this.gameController;
    }

    /**
     * Gets the ShipController.
     * 
     * @return the ship controller
     */
    public CharacterController getCharacter() {
        return this.firstPlayer;
    }

    /**
     * Gets the list of the EnemyControllers that are in the field.
     * 
     * @return the list of all enemy controllers
     */
    public synchronized List<EnemyController> getEnemies() {
        return new LinkedList<>(this.enemies);
    }

    /**
     * Gets the list of the EnemyBulletControllers that are in the field.
     * 
     * @return the list of all enemy bullet controllers
     */
    public synchronized List<BulletController> getEnemyBullets() {
        return new LinkedList<>(this.enemyBullets);
    }

    /**
     * Gets the list of the CharacterBulletControllers that are in the field.
     * 
     * @return the list of all character bullet controllers
     */
    public synchronized List<BulletController> getCharacterBullets() {
        return new LinkedList<>(this.characterBullets);
    }

    /**
     * Gets the list of the MeteorControllers that are in the field.
     * 
     * @return the list of all meteor controllers
     */
    public synchronized List<MeteorController> getMeteors() {
        return new LinkedList<>(this.meteors);
    }

    /**
     * Adds the enemy to the list of the EnemyControllers.
     * 
     * @param enemy the enemy to be added
     */
    public synchronized void addEnemy(final EnemyController enemy) {
        this.enemies.add(enemy);
        this.startAgent(new AntagonistsAgent(enemy, this.gameController));
    }

    /**
     * Adds the Bullet to the list of the EnemyBulletControllers.
     * 
     * @param enemyBullet the enemy bullet to be added
     */
    public synchronized void addEnemyBullet(final BulletController enemyBullet) {
        this.enemyBullets.add(enemyBullet);
        this.startAgent(new AntagonistsAgent(enemyBullet, this.gameController));
    }

    /**
     * Adds the bullet to the list of the CharacterBulletControllers.
     * 
     * @param characterBullet the character bullet to be added
     */
    public synchronized void addCharacterBullet(final BulletController characterBullet) {
        this.characterBullets.add(characterBullet);
        this.startAgent(new CharacterBulletAgent(characterBullet, this.gameController));
    }

    /**
     * Adds the meteor to the list of the MeteorControllers.
     * 
     * @param meteor the meteor to be added
     */
    public synchronized void addMeteor(final MeteorController meteor) {
        this.meteors.add(meteor);
        this.startAgent(new AntagonistsAgent(meteor, this.gameController));
    }

    /**
     * Removes a destroyed enemy from the list.
     * 
     * @param enemy the enemy destroyed
     */
    public synchronized void removeEnemy(final EnemyController enemy) {
        this.enemies.remove(enemy);
        this.drawAgent.addExplodingEntity(enemy);
    }

    /**
     * Removes a destroyed bullet of an enemy from the list.
     * 
     * @param enemyBullet the enemy's bullet destroyed
     */
    public synchronized void removeEnemyBullet(final BulletController enemyBullet) {
        this.enemyBullets.remove(enemyBullet);
    }

    /**
     * Removes a destroyed bullet of the character from the list.
     * 
     * @param characterBullet the character's bullet destroyed
     */
    public synchronized void removeCharacterBullet(final BulletController characterBullet) {
        this.characterBullets.remove(characterBullet);
    }

    /**
     * Removes a destroyed meteor from the list.
     * 
     * @param meteor the meteor destroyed
     */
    public synchronized void removeMeteor(final MeteorController meteor) {
        this.meteors.remove(meteor);
        this.drawAgent.addExplodingEntity(meteor);
    }

    /**
     * 
     * @param agent the start of the game.
     */
    protected void startAgent(final Thread agent) {
        agent.start();
    }

    /**
     * Gets the secondPlayer.
     * 
     * @return the second player controller.
     */
    public SecondCharacterController getSecondPlayer() {
        return this.secondPlayer;
    }

    public final CameraController getMultiCamera() {
        return this.multiplayerCamera;
    }
}
