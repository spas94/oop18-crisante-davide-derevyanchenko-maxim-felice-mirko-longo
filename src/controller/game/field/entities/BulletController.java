package controller.game.field.entities;

import controller.game.GameController;
import controller.multiplayer.Player;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import model.entity.Entity;
import model.entity.bullet.Bullet;
import model.entity.bullet.BulletImpl;
import model.game.LevelEnum;

/**
 * Controller class of Bullet.
 */
public class BulletController implements EntityController {

    private final Image image;
    private final GameController gameController;
    private final Bullet bullet;

    /**
     * Build a BulletController and his Bullet.
     * 
     * @param gameController the gameController.
     * @param level          the level of the Bullet to create.
     * @param src            the starting point of the Bullet to create.
     * @param target         the target point of the Bullet.
     * @param fieldSize      the field width and height.
     * @param shooter       the shooter of the bullet.

     */
    public BulletController(final GameController gameController, final LevelEnum level, final Point2D src,
            final Point2D target, final Dimension2D fieldSize, final Player shooter) {
        this.image =  new Image("file:///" + gameController.getAccount().getSettings().getResourceMapping().
                getBullets().get(level).toString());
        this.bullet = new BulletImpl(level, src, target, fieldSize, gameController, shooter);
        this.gameController = gameController;
    }

    /**
     * Build a BulletController and his easy-level Bullet.
     * 
     * @param gameController the gameController.
     * @param src            the starting point of the Bullet to create.
     * @param target         the target point of the Bullet.
     * @param fieldSize      the field width and height.
     *  @param shooter       the shooter of the bullet.
     */
    public BulletController(final GameController gameController, final Point2D src, final Point2D target,
            final Dimension2D fieldSize, final Player shooter) {
        this(gameController, LevelEnum.EASY, src, target, fieldSize, shooter);
    }

    /**
     * Get the damage the Bullet provokes when impacts with an enemy.
     * 
     * @return the health-points it subtracts to the enemy it hits.
     */
    public int getDamage() {
        return this.bullet.getDamage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void draw() {
        final double angle = Math.toDegrees(this.bullet.getAngle()) + 180;
        this.gameController.getFieldView().drawEntity(image, angle, this.bullet.getBoundary());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update() {
        this.bullet.update();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Entity getEntity() {
        return this.bullet;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {
        this.gameController.getFieldController().removeCharacterBullet(this);
        this.gameController.getFieldController().removeEnemyBullet(this);
    }

}
