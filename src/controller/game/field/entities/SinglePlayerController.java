package controller.game.field.entities;

import java.awt.MouseInfo;

import controller.game.GameController;
import controller.game.field.CameraController;
import controller.multiplayer.Player;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;

/**
 * 
 * 
 *
 */
public class SinglePlayerController extends CharacterController {

    private final Image shipImage;
    private final CameraController camController;
    private final boolean isMultiplayer;

    /**
     * 
     * @param gameController start the gameController
     * @param camController  control the single player with a camera controller.
     * 
     */
    public SinglePlayerController(final GameController gameController, final CameraController camController) {
        super(gameController, Player.FIRST);
        this.camController = camController;
        this.isMultiplayer = false;
        this.shipImage = new Image("file:///" + gameController.getAccount().getSettings().getImageName());

    }

    public SinglePlayerController(final GameController gameController, final Point2D initialPosition) {
        super(gameController, initialPosition, Player.FIRST);
        this.camController = null;
        this.shipImage = new Image("file:///" + gameController.getAccount().getSettings().getImageName());
        this.isMultiplayer = true;
    }

    @Override
    public final Point2D getUpdatedPosition() {
        final Point2D mouseOnScreen = new Point2D(MouseInfo.getPointerInfo().getLocation().getX(),
                MouseInfo.getPointerInfo().getLocation().getY());
        final Point2D mousePosition = this.getGameController().getFieldView().getCanvas().screenToLocal(mouseOnScreen);
        final Point2D vector = mousePosition.subtract(this.getResolution().getWidth() / 2,
                this.getResolution().getHeight() / 2);
        final long time = System.currentTimeMillis();
        final long timeFromLastUpdate = time - this.getLastUpdate();
        setLastUpdate(time);
        final double rad = Math.atan2(vector.getY(), vector.getX());
        setAngle(Math.toDegrees(rad));
        double shipUpdateX = this.getShip().getBoundary().getMinX()
                + (timeFromLastUpdate * this.getShip().getSpeed() * Math.cos(rad));
        double shipUpdateY = this.getShip().getBoundary().getMinY()
                + (timeFromLastUpdate * this.getShip().getSpeed() * Math.sin(rad));

        if (isMultiplayer) {
            if (shipUpdateX < 0) {
                shipUpdateX = this.getGameController().getFieldView().getCanvas().getWidth()
                        - this.getShip().getBoundary().getWidth();
            } else if (shipUpdateX > (this.getGameController().getFieldView().getCanvas().getWidth()
                    - this.getShip().getBoundary().getWidth())) {
                shipUpdateX = 0;
            }
            if (shipUpdateY < 0) {
                shipUpdateY = this.getGameController().getFieldView().getCanvas().getHeight()
                        - this.getShip().getBoundary().getHeight();
            } else if (shipUpdateY > (this.getGameController().getFieldView().getCanvas().getHeight()
                    - this.getShip().getBoundary().getHeight())) {
                shipUpdateY = 0;

            }
        } else {
            if (shipUpdateX < 0) {
                shipUpdateX = 0;
            } else if (shipUpdateX > (this.getGameController().getFieldView().getCanvas().getWidth()
                    - this.getShip().getBoundary().getWidth())) {
                shipUpdateX = this.getGameController().getFieldView().getCanvas().getWidth()
                        - this.getShip().getBoundary().getWidth();
            }
            if (shipUpdateY < 0) {
                shipUpdateY = 0;
            } else if (shipUpdateY > (this.getGameController().getFieldView().getCanvas().getHeight()
                    - this.getShip().getBoundary().getHeight())) {
                shipUpdateY = this.getGameController().getFieldView().getCanvas().getHeight()
                        - this.getShip().getBoundary().getHeight();
            }
        }

        return new Point2D(shipUpdateX, shipUpdateY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update() {
        final Point2D updatedPosition = getUpdatedPosition();
        if (isMultiplayer) {
            this.getGameController().getFieldController().getMultiCamera()
                    .setTranslation(new Point2D(updatedPosition.getX() - this.getShip().getBoundary().getMinX(),
                            updatedPosition.getY() - this.getShip().getBoundary().getMinY()));
        } else {
            this.camController
                    .setTranslation(new Point2D(updatedPosition.getX() - this.getShip().getBoundary().getMinX(),
                            updatedPosition.getY() - this.getShip().getBoundary().getMinY()));
        }
        this.getShip().update(updatedPosition.getX(), updatedPosition.getY());
    }

    @Override
    public final synchronized void shoot() {
        final Point2D mouseOnScreen = new Point2D(MouseInfo.getPointerInfo().getLocation().getX(),
                MouseInfo.getPointerInfo().getLocation().getY());
        final Point2D mousePosition = this.getGameController().getFieldView().getCanvas().screenToLocal(mouseOnScreen);
        final Point2D vector = mousePosition.subtract(this.getResolution().getWidth() / 2,
                this.getResolution().getHeight() / 2);
        final double rad = Math.toRadians(this.getAngle());
        final Point2D startingPoint = this.getShip().getCentralPosition().add(
                Math.cos(rad) * (this.getShip().getBoundary().getHeight() * 0.5),
                Math.sin(rad) * (this.getShip().getBoundary().getHeight() * 0.5));
        this.getGameController().getFieldController().addCharacterBullet(new BulletController(this.getGameController(),
                getBulletLevel(), startingPoint, startingPoint.add(vector), this.getResolution(), Player.FIRST));
    }

    @Override
    public final void draw() {
        this.getGameController().getFieldView().drawEntity(this.shipImage, getAngle(), getShip().getBoundary());
    }

    @Override
    public final Image getShipImage() {
        return this.shipImage;
    }

}
