package controller.game.field.entities;

import controller.game.GameController;
import controller.multiplayer.Player;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import model.entity.ship.charactership.CharacterShip;
import model.entity.ship.charactership.CharacterShipImpl;
import model.game.LevelEnum;
import model.game.Life;

/**
 * Class that controls the character ship moves.
 *
 */
public abstract class CharacterController implements EntityController {
    /**
     * level of the bullet.
     */
    private static final String EXTENSION = ".png";
    private final Dimension2D resolution;
    private final CharacterShip ship;
    private final GameController gameController;
    private double angle;
    private long lastUpdate;

    /**
     * Constructor with no fixed camera.
     * 
     * @param gameController  the view in which the ship is moving.
     * @param initialPosition the initial position on the ship.
     * @param player          the player this a character controller is assigned to. 
     */
    public CharacterController(final GameController gameController, final Point2D initialPosition, final Player player) {
        this.gameController = gameController;
        this.resolution = this.gameController.getAccount().getSettings().getResolution();
        this.ship = new CharacterShipImpl(initialPosition, this.resolution, player);
        this.lastUpdate = System.currentTimeMillis();
    }

    /**
     * Constructor with no fixed camera.
     * 
     * @param gameController the view in which the ship is moving
     * @param player          the player this a character controller is assigned to. 

     */
    public CharacterController(final GameController gameController, final Player player) {
        this.gameController = gameController;
        this.resolution = this.gameController.getAccount().getSettings().getResolution();
        this.ship = new CharacterShipImpl(new Point2D(this.resolution.getWidth() / 2, this.resolution.getHeight() / 2),
                this.resolution, player);
        this.lastUpdate = System.currentTimeMillis();
    }

    /**
     * 
     * @return the position of the ship.
     */
    public abstract Point2D getUpdatedPosition();

    /**
     * Gets the angle, in degrees, by which the ship is rotated.
     * 
     * @return the angle, in degrees, by which the ship is rotated
     */
    public double getAngle() {
        return this.angle;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract void draw(); 

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract void update();

    /**
     * {@inheritDoc}
     */
    @Override
    public CharacterShip getEntity() {
        return this.ship;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {
        this.gameController.setEnded(true);
    }

    /**
     * Method that gets the life of the character ship.
     * 
     * @return the Life of the character ship
     */
    public Life getLife() {
        return this.ship.getLife();
    }

    /**
     * Method that updates the moment time when the character ship was updated.
     * Needed when the game is paused.
     * 
     * @param lastUpdate the moment in which the ship has to restart to update
     */
    public synchronized void setLastUpdate(final long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * Return the last position of the ship.
     * 
     * @return the last position for the ship.
     */
    public synchronized long getLastUpdate() {
        return this.lastUpdate;
    }

    /**
     * Method that creates a character's bullet.
     */
    public abstract void shoot();

    /**
     * 
     * @return the gameController
     */
    public GameController getGameController() {
        return this.gameController;
    }

    /**
     * 
     * @return the resolution of the game
     */
    public Dimension2D getResolution() {
        return this.resolution;
    }

    /**
     * 
     * @return the level of bullet
     */
    public LevelEnum getBulletLevel() {
        return gameController.getGameLevel();
    }

    /**
     * 
     * @return the extension.
     */
    public static String getExtension() {
        return EXTENSION;
    }

    /**
     * 
     * @return the image of the ship
     */
    public abstract Image getShipImage();

    /**
     * 
     * @return the character of the ship
     */
    public CharacterShip getShip() {
        return ship;
    }
    /**
     * set the angle of the map.
     * 
     * @param angle return the angle of the map.
     */
    public void setAngle(final double angle) {
        this.angle = angle;
    }
}
