package controller.game;

import java.net.URL;
import java.util.ResourceBundle;
import controller.StageController;
import controller.menu.FXMLController;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import model.account.Account;
import utilities.ErrorLog;
import view.game.OverlayView;

/**
 * Controller class of Overlay.
 *
 */
public abstract class OverlayController implements FXMLController {
    /**
     * is a lives of the character ship.
     */
    public static final String LIVES = "LIVES";
    /**
     * the energy of the character ship .
     */
    public static final String HP = "HP";
    /**
     * the score of the character ship.
     */
    public static final String SCORE = "SCORE";
    /**
     * the waiting time of the thread.
     */
    public static final long WAITING_TIME = 20;
    private final OverlayView view;
    private final GameController gameController;
    private ResourceBundle bundle;
    @FXML
    private Label lives;
    @FXML
    private Label hp;
    @FXML
    private Label score;
    @FXML
    private Label powerUpLbl;
    @FXML
    private Label tempPowerUpLbl;

    /**
     * Build the OverlayController.
     * 
     * @param account         the account of the player
     * @param stageController the controller of the stage
     * @param gameController  the controller of the game
     * @param multiplayer     the multiplayer of the game
     */
    public OverlayController(final Account account, final StageController stageController,
            final GameController gameController, final boolean multiplayer) {
        this.gameController = gameController;
        this.view = new OverlayView(account, stageController, this, multiplayer);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(final URL url, final ResourceBundle bundle) {
        this.bundle = bundle;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start() {
        this.gameController.getGameView().getRoot().getChildren().add(this.view.getSubScene());
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!gameController.isEnded()) {
                    try {
                        if (!gameController.isInPause()) {

                            initLabelSystem();
                        }
                        Thread.sleep(WAITING_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();

                        ErrorLog.getLog().printError();
                        System.exit(0);
                    }
                }
            }
        }).start();
    }

    protected abstract void initLabelSystem();

    /**
     * Get the OverlayView.
     * 
     * @return the view
     */
    public OverlayView getView() {
        return this.view;
    }

    /**
     * Get the PowerUpLabel.
     * 
     * @return the powerUpLabel
     */
    public Label getPowerUpLabel() {
        return this.powerUpLbl;
    }

    /**
     * Get the TemporaryPowerUpLabel.
     * 
     * @return the tempPowerUpLabel
     */
    public Label getTempPowerUpLbl() {
        return this.tempPowerUpLbl;
    }

    public final GameController getGameController() {
        return gameController;
    }

    public final ResourceBundle getBundle() {
        return bundle;
    }

    public final Label getLives() {
        return lives;
    }

    public final Label getHp() {
        return hp;
    }

    public final Label getScore() {
        return score;
    }

    public final Label getPowerUpLbl() {
        return powerUpLbl;
    }

    public final void setTempPowerUpLbl(final Label tempPowerUpLbl) {
        this.tempPowerUpLbl = tempPowerUpLbl;
    }
}
