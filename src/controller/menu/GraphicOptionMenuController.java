package controller.menu;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import controller.StageController;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import model.account.Account;
import model.game.LevelEnum;
import utilities.AlertUtils;
import utilities.ErrorLog;
import utilities.FileUtils;
import utilities.GameUtils;
import utilities.ImageLoader;
import utilities.ResourceMapping;
import view.menu.GraphicMenuOptionView;

public class GraphicOptionMenuController implements FXMLController {

    private static final String BACKGROUD_LABEL_KEY = "backgroundLabel";
    private static final String ENEMY_LABEL_KEY = "enemyLabel";
    private static final String METEOR_LABEL_KEY = "meteorLabel";
    private static final String BULLET_LABEL_KEY = "bulletLabel";
    private static final String IMAGE_BUTTON_KEY = "imageButton";
    private static final String ENEMY_BUTTON_KEY = "enemyButton";
    private static final String METEOR_BUTTON_KEY = "meteorButton";
    private static final String BULLET_BUTTON_KEY = "bulletButton";
    private static final String SECOND_BUTTON_PLAYER_KEY = "buttonSecondPlayer";
    private static final String BACK_BUTTON_KEY = "backButton";
    private static final String LEVEL_LABEL_KEY = "levelLabel";
    private static final String SECOND_PLAYER_KEY = "secondPlayer";

    private ResourceBundle bundle;
    @FXML
    private Label backgroundLabel;
    @FXML
    private Label enemyLabel;
    @FXML
    private Label meteorLabel;
    @FXML
    private Label bulletLabel;
    @FXML
    private Label levelLabel;
    @FXML
    private Label secondPlayerLabel;
    @FXML
    private ImageView backgroundImage;
    @FXML
    private ImageView enemyImage;
    @FXML
    private ImageView meteorImage;
    @FXML
    private ImageView bulletImage;
    @FXML
    private ImageView secondPlayerImage;
    @FXML
    private Button buttonImage;
    @FXML
    private Button buttonEnemy;
    @FXML
    private Button buttonBullet;
    @FXML
    private Button buttonMeteor;
    @FXML
    private Button buttonSecondPlayer;
    @FXML
    private Button back;
    @FXML
    private ChoiceBox<LevelEnum> choiceLevel;
    private final StageController stageController;
    private final Account account;
    private final ResourceMapping rs;
    private final ImageLoader imageLoader;

    @FXML
    private GridPane grid;

    public GraphicOptionMenuController(final Account account, final StageController stageController) {
        this.imageLoader = new ImageLoader(stageController.getStage());
        this.account = account;
        this.stageController = stageController;
        this.rs = account.getSettings().getResourceMapping();
    }

    @Override
    public final void initialize(final URL location, final ResourceBundle resources) {
        this.bundle = resources;
        setLanguage();
        setComponents();
        showByLevel(choiceLevel.getValue());

    }

    /**
     * show the preview of the background.
     */
    public void importBackground() {
        final File file = imageLoader.loadImage();
        if (file != null) {
            showBackgroud(FileUtils.FILE_PREFIX + file.toString());
            rs.addBackgrounds(choiceLevel.getValue(), file);
        }
    };

    /**
     * show the preview of the meteor.
     */
    public void importMeteor() {
        final File file = imageLoader.loadImage();
        if (file != null) {
            showMeteor(FileUtils.FILE_PREFIX + file.toString());
            rs.addMeteors(choiceLevel.getValue(), file);
        }
    };

    /**
     * show the preview of the enemy.
     */
    public void importEnemy() {
        final File file = imageLoader.loadImage();
        if (file != null) {
            showEnemy(FileUtils.FILE_PREFIX + file.toString());
            rs.addEnemies(choiceLevel.getValue(), file);
        }
    };

    /**
     * show the preview of the bullet.
     */
    public void importBullet() {
        final File file = imageLoader.loadImage();
        if (file != null) {
            showBullet(FileUtils.FILE_PREFIX + file.toString());
            rs.addBullets(choiceLevel.getValue(), file);
        }
    };

    /**
     * show the preview of the background.
     */
    public void importSecondPlayer() {
        final File file = imageLoader.loadImage();
        if (file != null) {
            showSecondPlayer(FileUtils.FILE_PREFIX + file.toString());
            rs.addSecondPlayer(file);
        }
    };

    /**
     * show the graphic selected level.
     */
    public void showByLevel() {
        showByLevel(choiceLevel.getValue());
    }

    private void showByLevel(final LevelEnum level) {
        showBackgroud(FileUtils.FILE_PREFIX + rs.getBackgrounds().get(level).toString());
        showMeteor(FileUtils.FILE_PREFIX + rs.getMeteors().get(level).toString());
        showEnemy(FileUtils.FILE_PREFIX + rs.getEnemies().get(level).toString());
        showBullet(FileUtils.FILE_PREFIX + rs.getBullets().get(level).toString());
        showSecondPlayer(FileUtils.FILE_PREFIX + rs.getGuest().toString());

    }

    private void showBullet(final String image) {
        showComponents(bulletImage, image);
    }

    private void showEnemy(final String image) {
        showComponents(enemyImage, image);
    }

    private void showMeteor(final String image) {
        showComponents(meteorImage, image);
    }

    private void showBackgroud(final String image) {
        showComponents(backgroundImage, image);
    }

    private void showComponents(final ImageView image, final String path) {
        Platform.runLater(() -> image.setImage(new Image(path)));
    }

    /**
     * show the graphic selected level.
     * @param image the second player to be shown
     */
    public void showSecondPlayer(final String image) {
        showComponents(secondPlayerImage, image);
    }

    private void setLanguage() {

        this.backgroundLabel.setText(this.bundle.getString(BACKGROUD_LABEL_KEY));
        this.enemyLabel.setText(this.bundle.getString(ENEMY_LABEL_KEY));
        this.meteorLabel.setText(this.bundle.getString(METEOR_LABEL_KEY));
        this.bulletLabel.setText(this.bundle.getString(BULLET_LABEL_KEY));
        this.secondPlayerLabel.setText(this.bundle.getString(SECOND_PLAYER_KEY));
        this.buttonImage.setText(this.bundle.getString(IMAGE_BUTTON_KEY));
        this.buttonEnemy.setText(this.bundle.getString(ENEMY_BUTTON_KEY));
        this.buttonBullet.setText(this.bundle.getString(BULLET_BUTTON_KEY));
        this.buttonMeteor.setText(this.bundle.getString(METEOR_BUTTON_KEY));
        this.back.setText(this.bundle.getString(BACK_BUTTON_KEY));
        this.levelLabel.setText(this.bundle.getString(LEVEL_LABEL_KEY));
        this.buttonSecondPlayer.setText(this.bundle.getString(SECOND_BUTTON_PLAYER_KEY));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start() {
        this.stageController.setScene(new GraphicMenuOptionView(this.account, this).getScene());
    }

    /**
     * Method to go back to the option menu.
     */
    public void goBack() {
        this.grid.setEffect(GameUtils.getBlurEffect());
        final Optional<ButtonType> confirmSettings = AlertUtils.createConfirmOptionsDialog().showAndWait();
        if (confirmSettings.get() == ButtonType.YES) {
            try {

                FileUtils.printAccount(account);
                new OptionsController(this.account, this.stageController).start();
            } catch (IOException e) {
                e.printStackTrace();

                ErrorLog.getLog().printError();
                System.exit(0);
            }
        }
        this.grid.setEffect(GameUtils.getTransparentEffect());
    }

    private void setComponents() {
        this.choiceLevel.setItems(FXCollections.observableArrayList(LevelEnum.values()));
        this.choiceLevel.setValue(LevelEnum.EASY);
    }

}
