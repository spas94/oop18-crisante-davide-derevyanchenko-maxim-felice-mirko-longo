package controller.menu;

import controller.StageController;
import controller.game.GameController;
import controller.game.OverlayController;
import javafx.application.Platform;
import model.account.Account;

/**
 * The single player overlay.
 */
public class OverlaySinglePlayerController extends OverlayController {
    /**
     * 
     * @param account         the account.
     * @param stageController the Stage controller.
     * @param gameController  the game controller.
     * @param multiplayer     the multiplayer controller.
     */
    public OverlaySinglePlayerController(final Account account, final StageController stageController,
            final GameController gameController, final boolean multiplayer) {
        super(account, stageController, gameController, multiplayer);
    }

    @Override
    protected final void initLabelSystem() {
        Platform.runLater(() -> {
            getLives().setText(getBundle().getString(LIVES)
                    + Integer.toString(getGameController().getFieldController().getCharacter().getLife().getLives()));
            getHp().setText(getBundle().getString(HP) + Integer
                    .toString(getGameController().getFieldController().getCharacter().getLife().getCurrentHealth()));
            getScore().setText(
                    getBundle().getString(SCORE) + Integer.toString(getGameController().getScore().getScorePoints()));
        });
    }
}
