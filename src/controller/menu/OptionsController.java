package controller.menu;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

import controller.StageController;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Dimension2D;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import model.account.Account;
import utilities.AlertUtils;
import utilities.AudioLoader;
import utilities.ErrorLog;
import utilities.FileUtils;
import utilities.GameUtils;
import utilities.ImageLoader;
import utilities.ResourceMapping;
import view.menu.OptionsView;

/**
 * 
 * This class controls the Options view.
 *
 */
public class OptionsController implements FXMLController {

    private static final Dimension RESOLUTION = Toolkit.getDefaultToolkit().getScreenSize();
    private static final String BACK_KEY = "back";
    private static final String FIRST_CHOICE_CB1 = "1024x600";
    private static final String SECOND_CHOICE_CB1 = "1024x768";
    private static final String THIRD_CHOICE_CB1 = "1280x720";
    private static final String FOURTH_CHOICE_CB1 = "1366x768";
    private static final String FIFTH_CHOICE_CB1 = "1440x900";
    private static final String SIXTH_CHOICE_CB1 = "1680x1050";
    private static final String SEVENTH_CHOICE_CB1 = "1920x1080";
    private static final String EIGHT_CHOICE_CB1 = "2560x1440";
    private static final String NINTH_CHOICE_CB1 = "3840x2160";
    private static final String FIRST_CHOICE_CB2 = "it";
    private static final String SECOND_CHOICE_CB2 = "en";
    private static final String RESOLUTION_KEY = "resolution";
    private static final String LANGUAGE_KEY = "language";
    private static final String SOUND_KEY = "sound";
    private static final String YES_KEY = "yes";
    private static final String NO_KEY = "no";
    private static final String CHANGE_SHIP_KEY = "change_ship";
    private static final String CHANGE_CREDENTIALS_KEY = "changeCredentials";
    private static final String CHANGE_KEY = "change";
    private static final String CUSTOMIZE_KEY = "customize_grapich";
    private static final String AUDIO_MENU_KEY = "load_audio_menu";
    private static final String AUDIO_OPTION_KEY = "load_option_menu";
    private static final String AUDIO_GAME_KEY = "load_game_menu";
    private static final String CUSTOMIZE_BUTTON_KEY = "customize";
    private static final String IMAGE_LOAD_KEY = "load_ship_image";

    private static final ObservableList<String> RESOLUTIONS_LIST = FXCollections.observableArrayList(FIRST_CHOICE_CB1,
            SECOND_CHOICE_CB1, THIRD_CHOICE_CB1, FOURTH_CHOICE_CB1, FIFTH_CHOICE_CB1, SIXTH_CHOICE_CB1,
            SEVENTH_CHOICE_CB1, EIGHT_CHOICE_CB1, NINTH_CHOICE_CB1);
    private static final ObservableList<String> LANGUAGE_LIST = FXCollections.observableArrayList(FIRST_CHOICE_CB2,
            SECOND_CHOICE_CB2);

    private final Map<String, String> mapping;
    private final ObservableList<String> mappedValues;
    private final Account account;
    private final StageController stageController;
    private ResourceBundle bundle;
    private final AudioLoader audioLoader;
    private final ImageLoader imageLoader;
    private final ResourceMapping rs;
    @FXML
    private ChoiceBox<String> language;
    @FXML
    private ChoiceBox<String> resolution;
    @FXML
    private ChoiceBox<String> shipList;
    @FXML
    private GridPane grid;
    @FXML
    private GridPane gridAudio;
    @FXML
    private Button back;
    @FXML
    private Button changeBtn;
    @FXML
    private Label resolutionLb;
    @FXML
    private Label languageLb;
    @FXML
    private Label soundLb;
    @FXML
    private Label changeShipLb;
    @FXML
    private Label changeLbl;
    @FXML
    private Label customizeGraphicLabel;
    @FXML
    private RadioButton yes;
    @FXML
    private RadioButton no;
    @FXML
    private ImageView image;
    @FXML
    private Button changeAudioMenu;
    @FXML
    private Button browseImage;
    @FXML
    private Button changeAudioOption;
    @FXML
    private Button changeAudioGame;
    @FXML
    private Button customizeButton;

    /**
     * Build the OptionsController.
     * 
     * @param account         the game account.
     * @param stageController the stage controller.
     */
    public OptionsController(final Account account, final StageController stageController) {
        this.account = account;
        this.stageController = stageController;
        this.audioLoader = new AudioLoader(stageController.getStage());
        this.imageLoader = new ImageLoader(stageController.getStage());
        this.rs = account.getSettings().getResourceMapping();
        this.mapping = new HashMap<>();
        initMapping();
        mappedValues = FXCollections.observableArrayList(mapping.keySet());
    }

    private void initMapping() {
        String cleanName = removeExtension(account.getSettings().getImageName());
        cleanName = cleanPathToFileName(cleanName);
        mapping.put("RedFury", Paths.get(System.getProperty("user.dir"), "res", "images", "RedFury.png").toString());
        mapping.put("NightMare",
                Paths.get(System.getProperty("user.dir"), "res", "images", "NightMare.png").toString());
        mapping.put("GreenEvil",
                Paths.get(System.getProperty("user.dir"), "res", "images", "GreenEvil.png").toString());

        mapping.put(cleanName, account.getSettings().getImageName());
    }

    private String cleanPathToFileName(final String input) {
        String output;
        final String searchee = File.separator;
        final int index = input.lastIndexOf(searchee);
        output = input.substring(index + 1, input.length());
        return output;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        this.bundle = resources;
        setLanguage();
        setComponents();
        showLoadCharacterShip(image, account.getSettings().getImageName());
    }

    /**
     * Method to go back to the menu.
     */
    @FXML
    public void goBack() {
        this.grid.setEffect(GameUtils.getBlurEffect());
        final Optional<ButtonType> confirmSettings = AlertUtils.createConfirmOptionsDialog().showAndWait();
        if (confirmSettings.get() == ButtonType.YES) {
            try {
                final String[] values = resolution.getValue().split("x");
                final Dimension2D selectedResolution = new Dimension2D(Double.parseDouble(values[0]),
                        Double.parseDouble(values[1]));
                if (selectedResolution.getWidth() <= RESOLUTION.getWidth()
                        || selectedResolution.getHeight() <= RESOLUTION.getHeight()) {
                    this.account.getSettings().setResolution(selectedResolution);
                }
                this.account.getSettings().setLanguage(language.getValue());
                if (yes.isSelected()) {
                    account.getSettings().setSound(true);
                } else if (no.isSelected()) {
                    account.getSettings().setSound(false);
                }
                FileUtils.printAccount(account);
                new MenuController(this.account, this.stageController).start();
            } catch (IOException e) {
                e.printStackTrace();

                ErrorLog.getLog().printError();
                System.exit(0);
            }
        }
        this.grid.setEffect(GameUtils.getTransparentEffect());
    }

    /**
     * 
     */
    @FXML
    public void customize() {
        new GraphicOptionMenuController(this.account, this.stageController).start();
    }

    /**
     * this method let check the change credentials scene.
     */
    public void changeCredentials() {
        new ChangeCredentialsController(this.account, this.stageController).start();
    }

    /**
     * show the game audio of the game.
     */
    public void importGameAudio() {
        final File file = audioLoader.loadAudio();
        if (file != null) {
            rs.addAudioGame(file);
            GameUtils.setLevelAudio(file);
        }
    };

    /**
     * show the option game audio of the game.
     */
    public void importOptionAudio() {
        final File file = audioLoader.loadAudio();
        if (file != null) {
            rs.addAudioOption(file);
            GameUtils.setSurvivalAudio(file);
        }
    };

    /**
     * show the menu game audio of the game.
     */
    public void importMenuAudio() {
        final File file = audioLoader.loadAudio();
        if (file != null) {
            rs.addAudioMenu(file);
            GameUtils.setMenuAudio(file);
            GameUtils.muteAllSounds();
            if (account.getSettings().isSoundOn()) {
                GameUtils.getMenuMusic().loop();
            }
        }
    };

    /**
     * load the image of character ship.
     */
    public void importCharacterShipImage() {
        final File file = imageLoader.loadImage();
        if (file != null) {
            final String name = removeExtension(file.getName());
            mapping.put(name, file.getAbsolutePath());
            mappedValues.add(name);
            showLoadCharacterShip(image, file.getAbsolutePath());
            this.account.getSettings().setImageName(file.getAbsolutePath());
            this.shipList.setValue(name);
        }
    };

    private String removeExtension(final String input) {
        String output;
        final int pointIndex = input.indexOf('.');
        output = input.substring(0, pointIndex);
        return output;
    }

    private void showLoadCharacterShip(final ImageView image, final String path) {
        Platform.runLater(() -> image.setImage(new Image("file:///" + path)));
    }

    public final void showSelection() {
        showLoadCharacterShip(image, mapping.get(shipList.getValue()));
        this.account.getSettings().setImageName(mapping.get(shipList.getValue()));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start() {
        this.stageController.setScene(new OptionsView(this.account, this).getScene());
    }

    private void setLanguage() {
        this.back.setText(this.bundle.getString(BACK_KEY));
        this.resolutionLb.setText(this.bundle.getString(RESOLUTION_KEY));
        this.languageLb.setText(this.bundle.getString(LANGUAGE_KEY));
        this.customizeGraphicLabel.setText(this.bundle.getString(CUSTOMIZE_KEY));
        this.soundLb.setText(this.bundle.getString(SOUND_KEY));
        this.yes.setText(this.bundle.getString(YES_KEY));
        this.no.setText(this.bundle.getString(NO_KEY));
        this.changeShipLb.setText(this.bundle.getString(CHANGE_SHIP_KEY));
        this.changeBtn.setText(this.bundle.getString(CHANGE_KEY));
        this.changeLbl.setText(this.bundle.getString(CHANGE_CREDENTIALS_KEY));
        this.changeAudioMenu.setText(this.bundle.getString(AUDIO_MENU_KEY));
        this.changeAudioOption.setText(this.bundle.getString(AUDIO_OPTION_KEY));
        this.changeAudioGame.setText(this.bundle.getString(AUDIO_GAME_KEY));
        this.browseImage.setText(this.bundle.getString(IMAGE_LOAD_KEY));
        this.customizeButton.setText(this.bundle.getString(CUSTOMIZE_BUTTON_KEY));

    }

    private void setComponents() {
        final String name = removeExtension(cleanPathToFileName(this.account.getSettings().getImageName()));
        this.language.setItems(LANGUAGE_LIST);
        this.resolution.setItems(RESOLUTIONS_LIST);
        this.shipList.setItems(mappedValues);
        this.shipList.setValue(name);
        showLoadCharacterShip(image, mapping.get(name));
        this.language.setValue(this.account.getSettings().getLanguage());
        this.resolution.setValue((int) this.account.getSettings().getResolution().getWidth() + "x"
                + (int) this.account.getSettings().getResolution().getHeight());
        if (this.account.getSettings().isSoundOn()) {
            this.yes.setSelected(true);
        } else {
            this.no.setSelected(true);
        }
    }
}
