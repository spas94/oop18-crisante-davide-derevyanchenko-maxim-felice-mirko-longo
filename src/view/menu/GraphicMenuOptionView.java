
package view.menu;

import java.io.IOException;
import java.util.ResourceBundle;

import controller.menu.GraphicOptionMenuController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import model.account.Account;
import utilities.SystemUtils;
import view.AbstractView;

/**
 * View of Options.
 *
 */
public class GraphicMenuOptionView extends AbstractView {

    private static final String OPTIONS_BUNDLE = "menu.GraphicOptionMenu";
    private static final String OPTIONS_VIEW = "OptionGraphicMenu.fxml";
    private final double prefWidth;
    private final double prefHeight;
    private final FXMLLoader loader;

    /**
     * Build a OptionsView.
     * 
     * @param account           the game account
     * @param graphicOptionMenuController the controller of this view
     */
    public GraphicMenuOptionView(final Account account, final GraphicOptionMenuController graphicOptionMenuController) {
        this.prefWidth = account.getSettings().getResolution().getWidth();
        this.prefHeight = account.getSettings().getResolution().getHeight();
        SystemUtils.setLocale(account.getSettings().getLanguage());
        this.loader = new FXMLLoader(ClassLoader.getSystemResource(OPTIONS_VIEW),
                ResourceBundle.getBundle(OPTIONS_BUNDLE));
        this.loader.setController(graphicOptionMenuController);
        super.init();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Parent getRoot() throws IOException {
        return this.loader.load();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double getWidth() {
        return this.prefWidth;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double getHeight() {
        return this.prefHeight;
    }
}
